package com.onwardsmg.upload;

import java.io.File;
import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	String token = "4916B460D8A609BAE0969613B57304035B519B6F41E25A9B69D6B3C3C5C07156";
    	// create http client
    	CloseableHttpClient httpclient = HttpClients.createDefault();
    	// create http post object
    	HttpPost httpPost = new HttpPost("http://localhost:3001/upload");
    	// add header	
    	httpPost.addHeader("authorization", "Bearer " + token);
    	// build the post body
    	HttpEntity body = MultipartEntityBuilder
    			.create()
    			.addTextBody("filename", "asdf")
    			.addBinaryBody("video",  new File("C://Users//user//Documents//test.iso"))
    			.build();
    	httpPost.setEntity(body);
    	
    	CloseableHttpResponse response = null;
    	try {
    		// execute request
			response = httpclient.execute(httpPost);
			// get response
			String test = EntityUtils.toString(response.getEntity());
			// if all ok, server should return id
			System.out.println(test);
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				response.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    	
    	
        
    }
}
